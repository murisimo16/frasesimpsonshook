import React from "react";

const Frase = (props) => {
  return (
    <div className="card mb-3">
      <div className="row no-gutters">
        <div className="col-md-3 text-center">
          <img src={props.fraseSimpsons.image} className="card-img" alt={props.fraseSimpsons.character} />
        </div>
        <div className="col-md-9 d-flex flex-column justify-content-center">
          <div className="card-body text-center">
            <h5 className="card-title">{props.fraseSimpsons.character}</h5>
            <p className="card-text">
              {props.fraseSimpsons.quote}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Frase;
