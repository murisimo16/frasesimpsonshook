import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Frase from "./components/Frase";
import logo from "./img/logosimpsons.png";
import Spinner from "./components/Spinner";

function App() {
  const [fraseSimpsons, setFraseSimpsons] = useState({});
  const [loader, setLoader] = useState(false);

  const consultarApi = async () => {
    setLoader(true);
    const api = await fetch("https://thesimpsonsquoteapi.glitch.me/quotes", {
      mode: "cors",
    });
    const respuesta = await api.json();
    console.log(api);
    console.log(respuesta[0]);

    setTimeout(() => {
      setFraseSimpsons(respuesta[0]);
      setLoader(false);
    }, 2000);
  };

  useEffect(() => {
    consultarApi();
  }, []);

  const cargarComponente = loader ? (
    <Spinner></Spinner>
  ) : (
    <Frase fraseSimpsons={fraseSimpsons}></Frase>
  );

  return (
    <section className="container">
      <article className="d-flex flex-column align-items-center">
        <img src={logo} alt="logo de los simpsons" className="w-75"></img>
        <button
          className="btn btn-warning my-4 w-50 shadow"
          onClick={() => consultarApi()}
        >
          Obtener frase
        </button>
      </article>
      {cargarComponente}
    </section>
  );
}

export default App;
